package javafx.tictactoe;

import java.util.Random;

import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.tictactoe.componets.*;
import javafx.scene.paint.Color;

public class MainLayout extends VBox implements EventHandler<Event> {
    Label label;
    GridPane grid;
    VBox pane;
    MainButton btn[] = new MainButton[9];

    ResetButton reset = new ResetButton("Reset");

    int cols;
    int rows;

    boolean player = true;

    public MainLayout() {
        cols = rows = 0;
        initUI();
        firstTurn();
    }

    private void initUI() {
        pane = new VBox();
        pane.prefHeight(128);
        pane.setMaxHeight(256);
        pane.setAlignment(Pos.CENTER);
        VBox.setMargin(pane, new Insets(10, 0, 12, 0));

        label = new Label();
        label.getStyleClass().setAll("h1", "text-center");
        label.setAlignment(Pos.CENTER);
        label.setContentDisplay(ContentDisplay.CENTER);

        reset.setOnAction(event -> handle(event));

        pane.getChildren().add(label);
        pane.getChildren().add(reset);

        grid = new GridPane();
        grid.setPadding(new Insets(2, 2, 2, 2));
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPrefSize(640, 345);

        setMargin(grid, new Insets(4, 10, 8, 10));

        for (int i = 0; i < 3; i++) {
            ColumnConstraints column = new ColumnConstraints();
            column.setHgrow(Priority.SOMETIMES);
            column.setMinWidth(10);
            column.setPrefWidth(345);

            RowConstraints row = new RowConstraints();
            row.setVgrow(Priority.SOMETIMES);
            row.setMinHeight(10);
            row.setPrefHeight(30);

            grid.getColumnConstraints().add(column);
            grid.getRowConstraints().add(row);
        }

        for (int i = 0; i < 9; i++)
            btn[i] = new MainButton("");

        for (int i = 0; i < 9; i++) {
            if (cols >= 3) {
                rows++;
                cols = 0;
            }
            grid.add(btn[i], cols, rows);
            btn[i].setOnAction(event -> handle(event));
            cols++;
        }

        this.getChildren().add(pane);
        this.getChildren().add(grid);
    }

    @Override
    public void handle(Event event) {
        for (int i = 0; i < 9; i++) {
            if (event.getSource() == btn[i]) {
                if (player) {
                    if (btn[i].getText() == "") {
                        player = false;
                        btn[i].setText("X");
                        label.setText("O's turn");
                        check();
                        draw();
                    }
                } else {
                    if (btn[i].getText() == "") {
                        player = true;
                        btn[i].setText("O");
                        label.setText("X's turn");
                        check();
                        draw();
                    }
                }
            } else if (event.getSource() == reset) {
                btn[i].reset();
                btn[i].setText("");
                btn[i].setEnable(true);
                btn[i].setDefualtStyle();
                firstTurn();

            }
        }
    }

    private void check() {
        // Checking for X

        // Horizontal for X
        if (btn[0].getText() == "X" &&
                btn[1].getText() == "X" &&
                btn[2].getText() == "X")
            winner_X(0, 1, 2);

        else if (btn[3].getText() == "X" &&
                btn[4].getText() == "X" &&
                btn[5].getText() == "X")
            winner_X(3, 4, 5);

        else if (btn[6].getText() == "X" &&
                btn[7].getText() == "X" &&
                btn[8].getText() == "X")
            winner_X(6, 7, 8);

        // Vertical for X
        else if (btn[0].getText() == "X" &&
                btn[3].getText() == "X" &&
                btn[6].getText() == "X")
            winner_X(0, 3, 6);

        else if (btn[1].getText() == "X" && btn[4].getText() == "X"
                && btn[7].getText() == "X")
            winner_X(1, 4, 7);

        else if (btn[2].getText() == "X" && btn[5].getText() == "X"
                && btn[8].getText() == "X")
            winner_X(2, 5, 8);

        // Diagonal for X
        else if (btn[0].getText() == "X" &&
                btn[4].getText() == "X" &&
                btn[8].getText() == "X")
            winner_X(0, 4, 8);

        else if (btn[2].getText() == "X" &&
                btn[4].getText() == "X" &&
                btn[6].getText() == "X")
            winner_X(2, 4, 6);

        // Checking for O

        // Horizontal for O
        if (btn[0].getText() == "O" &&
                btn[1].getText() == "O" &&
                btn[2].getText() == "O")
            winner_O(0, 1, 2);

        else if (btn[3].getText() == "O" &&
                btn[4].getText() == "O" &&
                btn[5].getText() == "O")
            winner_O(3, 4, 5);

        else if (btn[6].getText() == "O" &&
                btn[7].getText() == "O" &&
                btn[8].getText() == "O")
            winner_O(6, 7, 8);

        // Vertical for O
        else if (btn[0].getText() == "O" &&
                btn[3].getText() == "O" &&
                btn[6].getText() == "O")
            winner_O(0, 3, 6);

        else if (btn[1].getText() == "O" &&
                btn[4].getText() == "O" &&
                btn[7].getText() == "O")
            winner_O(1, 4, 7);

        else if (btn[2].getText() == "O"
                && btn[5].getText() == "O"
                && btn[8].getText() == "O")
            winner_O(2, 5, 8);

        // Diagonal for O
        else if (btn[0].getText() == "O" &&
                btn[4].getText() == "O" &&
                btn[8].getText() == "O")
            winner_O(0, 4, 8);

        else if (btn[2].getText() == "O" &&
                btn[4].getText() == "O" &&
                btn[6].getText() == "O")
            winner_O(2, 4, 6);
    }

    private void winner_X(int a, int b, int c) {
        for (int i = 0; i < 9; i++) {
            if (i != a && i != b && i != c) {
                btn[i].setEnable(false);
            } else {
                btn[i].setTextFill(Color.BLACK);
                btn[i].setGreenStyle();
            }
        }
        label.setText("X wins");
    }

    private void winner_O(int a, int b, int c) {
        for (int i = 0; i < 9; i++) {
            if (i != a && i != b && i != c) {
                btn[i].setEnable(false);
            } else {
                btn[i].setTextFill(Color.BLACK);
                btn[i].setGreenStyle();
            }
        }

        label.setText("O wins");
    }

    private void draw() {
        boolean emptyButtonExists = false;
        for (int i = 0; i < 9; i++) {
            if (btn[i].getText() == "") {
                emptyButtonExists = true;
                break;
            }
        }
        if (!emptyButtonExists) {
            label.setText("Draw!");
            for (int i = 0; i < 9; i++) {
                btn[i].setEnable(false);
            }
        }
    }

    private void firstTurn() {
        if (new Random().nextInt(2) == 0) {
            player = true;
            label.setText("X's turn");
        } else {
            player = false;
            label.setText("O's turn");
        }
    }
}
