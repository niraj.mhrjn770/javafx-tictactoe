package javafx.tictactoe.componets;

import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.enums.ButtonType;
import javafx.geometry.Pos;

public class ResetButton extends MFXButton {
    public ResetButton(String str) {
        super(str);
        super.setAlignment(Pos.CENTER);
        super.setStyle("-fx-font-size: 12px;");
        init();
    }

    private void init() {
        this.setFocusTraversable(false);
        this.setFocused(false);
        this.setRippleAnimateShadow(true);
        this.setRippleAnimationSpeed(1);
        this.setRippleBackgroundOpacity(.25);
        this.buttonTypeProperty().set(ButtonType.FLAT);
        this.lineSpacingProperty().set(2.5);
        this.autosize();
    }
}
