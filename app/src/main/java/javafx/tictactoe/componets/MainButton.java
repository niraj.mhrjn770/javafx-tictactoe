package javafx.tictactoe.componets;

import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.effects.DepthLevel;
import io.github.palexdev.materialfx.enums.ButtonType;
import javafx.geometry.Pos;
import javafx.scene.layout.Background;
import javafx.scene.paint.Color;

public class MainButton extends MFXButton {
    public MainButton(String str) {
        super(str);
        super.setAlignment(Pos.CENTER);
        super.setBackground(Background.fill(Color.WHITESMOKE));
        setDefualtStyle();

        init();
    }

    public void setDefualtStyle() {
        this.setStyle("-fx-font-size: 40px;-fx-text-fill:black;");
    }

    public void setGreenStyle() {
        this.setStyle("-fx-font-size: 40px;-fx-text-fill:black;" +
                "-fx-background-color: green;");
    }

    private void init() {
        this.setScaleX(1.0);
        this.setScaleY(1.0);
        this.setFocusTraversable(false);
        this.setFocused(false);
        this.setMaxSize(250, 125);
        this.setRippleAnimateShadow(true);
        this.setRippleAnimationSpeed(1);
        this.setRippleBackgroundOpacity(.25);
        this.buttonTypeProperty().set(ButtonType.RAISED);
        this.depthLevelProperty().set(DepthLevel.LEVEL2);
        this.lineSpacingProperty().set(1.5);
        this.autosize();
    }

    public void setEnable(boolean flag) {
        if (flag == false) {
            this.setDisable(true);
            this.setStyle("-fx-background-color: #acacac;-fx-font-size:40px;-fx-text-fill: #666666;");
        } else {
            setDefualtStyle();
            this.setDisable(false);
        }
    }

    public void reset() {
        this.getChildren().clear();
    }

}
